package org.zjvis.dp.data.lineage;

import com.google.common.collect.Lists;
import java.util.List;
import java.util.Map;
import javax.annotation.Resource;
import org.apache.commons.collections.CollectionUtils;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.zjvis.dp.data.lineage.data.FieldInfo;
import org.zjvis.dp.data.lineage.data.FieldLineageInfo;
import org.zjvis.dp.data.lineage.data.Place;
import org.zjvis.dp.data.lineage.enums.SQLType;
import org.zjvis.dp.data.lineage.parser.DataLineageParser;
import org.zjvis.dp.data.lineage.util.DataLineageUtil;

@SpringBootTest
@RunWith(SpringRunner.class)
class LineageApplicationTests {

    @Resource
    private DataLineageParser dataLineageParser;

    @Test
    void contextLoads() {
    }

    @Test
    void testClickhouse() {
        String sql = "INSERT INTO table_a(field_1, field_2)\n"
                + "SELECT \n"
                + "  field_1,\n"
                + "  field_2\n"
                + "FROM\n"
                + "(\n"
                + "  SELECT \n"
                + "    table_b.field_1,\n"
                + "    table_b.field_2\n"
                + "  FROM table_b\n"
                + "  INNER JOIN\n"
                + "  (\n"
                + "  \tSELECT\n"
                + "    \tfield_1,\n"
                + "    \tfield_2\n"
                + "  \tFROM table_c\n"
                + "  ) c \n"
                + "  ON table_b.field_1 = c.field_1\n"
                + "  INNER JOIN\n"
                + "  (\n"
                + "  \tSELECT\n"
                + "    \tfield_1,\n"
                + "    \tfield_2\n"
                + "   \tFROM table_d\n"
                + "   ) d\n"
                + "   ON table_b.field_1 = d.field_1\n"
                + ");";
        List<FieldLineageInfo> fieldLineageInfoList = dataLineageParser.processFieldLineageParse(
                SQLType.CLICKHOUSE.name(),
                sql,
                null
        );
        if (CollectionUtils.isNotEmpty(fieldLineageInfoList)) {
            for (FieldLineageInfo fieldLineageInfo : fieldLineageInfoList) {
                System.out.println(fieldLineageInfo);
            }
        }
    }

    @Test
    void testMysql() {
        String sql = "INSERT INTO student\n"
                + "(id, name, school_name)\n"
                + "SELECT\n"
                + "\tstudent_test.id,\n"
                + "\tstudent_test.name,\n"
                + "\tschool.name\n"
                + "from student_test\n"
                + "global join school\n"
                + "on student_test.school_id = school.id;";
        List<FieldLineageInfo> fieldLineageInfoList = dataLineageParser.processFieldLineageParse(SQLType.MYSQL.name(), sql, null);
        if (CollectionUtils.isNotEmpty(fieldLineageInfoList)) {
            for (FieldLineageInfo fieldLineageInfo : fieldLineageInfoList) {
                System.out.println(fieldLineageInfo);
            }
        }
    }

    @Test
    void testGrammarCheck() {
        String sql = "INSERT INTO student\n"
                + "SELECT\n"
                + "    student_test.name,\n"
                + "    school.name,\n"
                + "from student_test\n"
                + "global join school\n"
                + "on student_test.school_id = school.id;";
        dataLineageParser.grammarCheck(SQLType.MYSQL.name(), sql);
    }

    @Test
    void testExtractFieldPlace() {
        String sql = "INSERT INTO money_laundering_predict\n"
                + "SELECT \n"
                + "\tuser_id,\n"
                + "\tcard_id,\n"
                + "\tCASE \n"
                + "\t\tWHEN is_multi_ip = 0 THEN 0\n"
                + "\t\tWHEN is_night_trade = 0 THEN 0\n"
                + "\t\tWHEN is_deposity_transfer = 0 THEN 0\n"
                + "\t\tWHEN counterparty_num < 10 THEN 0\n"
                + "\t\tWHEN trade_num < 20 THEN 0\n"
                + "\t\tWHEN age_type != 'young' THEN 0\n"
                + "\t\tWHEN job_type != 'student' THEN 0\n"
                + "\t\tELSE 1\n"
                + "\tEND\n"
                + "FROM money_laundering_feature;";

        List<List<Place>> result = DataLineageUtil.extractFieldPlace(sql, 0);

        System.out.println("ok");
    }

    @Test
    void testGetPlaceFieldInfoMap() {
        String sql = "INSERT INTO table_a(field_1, field_2)\n"
                + "SELECT \n"
                + "  field_1,\n"
                + "  field_2\n"
                + "FROM\n"
                + "(\n"
                + "  SELECT \n"
                + "    table_b.field_1,\n"
                + "    table_b.field_2\n"
                + "  FROM table_b\n"
                + "  INNER JOIN\n"
                + "  (\n"
                + "  \tSELECT\n"
                + "    \tfield_1,\n"
                + "    \tfield_2\n"
                + "  \tFROM table_c\n"
                + "  ) c \n"
                + "  ON table_b.field_1 = c.field_1\n"
                + "  INNER JOIN\n"
                + "  (\n"
                + "  \tSELECT\n"
                + "    \tfield_1,\n"
                + "    \tfield_2\n"
                + "   \tFROM table_d\n"
                + "   ) d\n"
                + "   ON table_b.field_1 = d.field_1\n"
                + ");";
        Map<Place, List<FieldInfo>> result = dataLineageParser.getPlaceFieldInfoMap(
                SQLType.CLICKHOUSE.name(),
                Lists.newArrayList(sql),
                Lists.newArrayList(0),
                null
        );
        System.out.println("ok");
    }

}
