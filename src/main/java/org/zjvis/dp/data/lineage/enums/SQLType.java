package org.zjvis.dp.data.lineage.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author zhouyu
 * @create 2023-05-31 15:11
 */
@Getter
@AllArgsConstructor
public enum SQLType {
    CLICKHOUSE,

    MYSQL,

    POSTGRES;
}
