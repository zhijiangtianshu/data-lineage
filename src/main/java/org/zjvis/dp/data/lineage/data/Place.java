package org.zjvis.dp.data.lineage.data;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author zhouyu
 * @create 2023-10-31 17:00
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Place {
    private Position from;
    private Position to;

    private String fieldStr;

    public Place(int line, int startPosition, int endPosition, String fieldStr) {
        from = Position.builder()
                .line(line)
                .ch(startPosition)
                .build();

        to = Position.builder()
                .line(line)
                .ch(endPosition)
                .build();

        this.fieldStr = fieldStr;
    }
}