package org.zjvis.dp.data.lineage.data;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author zhouyu
 * @create 2023-11-02 18:15
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Position {
    private Integer line;
    private Integer ch;
    private char character;
}