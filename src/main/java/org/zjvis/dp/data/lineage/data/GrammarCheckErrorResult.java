package org.zjvis.dp.data.lineage.data;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author zhouyu
 * @create 2023-08-17 15:55
 */
@AllArgsConstructor
@Data
@Builder
@NoArgsConstructor
public class GrammarCheckErrorResult {
    public boolean grammarError = Boolean.FALSE;
    /**
     * 所在行
     */
    public int lineNumber;

    /**
     * 所在行字符
     */
    public int indexOfLine;

    /**
     * 错误信息
     */
    public String errorMessage;
}
