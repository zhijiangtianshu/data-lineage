package org.zjvis.dp.data.lineage.parser;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.antlr.v4.runtime.BaseErrorListener;
import org.antlr.v4.runtime.RecognitionException;
import org.antlr.v4.runtime.Recognizer;

/**
 * @author zhouyu
 * @create 2023-08-17 15:25
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class GrammarErrorListener extends BaseErrorListener {
    private boolean grammarError = Boolean.FALSE;
    /**
     * 所在行
     */
    private int lineNumber;

    /**
     * 所在行字符
     */
    private int indexOfLine;

    /**
     * 错误信息
     */
    private String errorMessage;


    @Override
    public void syntaxError(
            Recognizer<?, ?> recognizer,
            Object offendingSymbol,
            int line,
            int charPositionInLine,
            String msg,
            RecognitionException e
    ) {
        this.grammarError = Boolean.TRUE;
        this.lineNumber = line;
        this.indexOfLine = charPositionInLine;
        this.errorMessage = msg;
    }
}
