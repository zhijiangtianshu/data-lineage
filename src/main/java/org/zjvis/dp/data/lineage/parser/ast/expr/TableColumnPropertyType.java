package org.zjvis.dp.data.lineage.parser.ast.expr;

public enum TableColumnPropertyType {
    ALIAS,
    CODEC,
    COMMENT,
    DEFAULT,
    MATERIALIZED,
    TTL
}
