package org.zjvis.dp.data.lineage.enums;

/**
 * @author zhouyu
 * @create 2023-07-10 15:39
 */
public enum TableChangeType {
    INSERT,

    ALTER,

    CREATE;
}
