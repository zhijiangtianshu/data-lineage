package org.zjvis.dp.data.lineage.enums;

/**
 * @author zhouyu
 * @create 2023-07-12 10:24
 */
public enum DataType {
    NUMBER,
    STRING,
    DATE,
    ENUM,
    ARRAY,
    TUPLE,
    UNKNOWN
}
